# GitLab SAST + DAST pipeline demo
A GitLab SAST + DAST pipeline demo with DevSecOps best practices (SAST, DAST and Container scan) using GitLab Security tooling. See pipeline result on [GitLab](https://gitlab.com/alexgracianoarj/gitlab-pipeline-demo/-/pipelines).
